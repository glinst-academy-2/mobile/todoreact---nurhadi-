import React, {Component} from 'react'

class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      list : [],
      inputField : "",
      editField : "",
      formEdit : true
    }
  }
  
  handleAdd(){
    let newList = [...this.state.list, this.state.inputField]
    this.setState({
      list : newList
    })
  }

  
  render(){
    console.log(this.state.list)
    return(
      <div className="App">
        <header className="App-header ">
          <h1>TODO APP</h1>
          <h6>with React</h6>
        </header>
        <div>
          <input
          type="input"
          onChange={(event)=>this.setState({inputField:event.target.value})}
          style={{display: this.state.formEdit? 'inline':'none'}}/>
          <button
          onClick={()=>this.handleAdd()}
          style={{display: this.state.formEdit? 'inline':'none'}}>ADD</button>
        </div>
        <div>
          {this.state.list.map((list,index) =>
            <div key={index}>
              <span>{list}</span>
            </div>
            )}
        </div>
      </div>
    )
  }
}
export default App;